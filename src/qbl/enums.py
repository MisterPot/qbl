from enum import (
    Enum,
)


class StrEnum(str, Enum):

    def __str__(self):
        return self.value

    __repr__ = __str__


class Location(StrEnum):

    """
    Data location in request body
    """

    HEADERS = 'headers'
    PARAMS = 'params'
    JSON = 'json'
    DATA = 'data'
    URL = 'url'
