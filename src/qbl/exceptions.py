
class ValidationError(Exception):
    """
    Raises, when field value not pass a validator
    """


class MissingMetaData(Exception):
    """
    Raises, when a QueryBuilder missing key data
    """


class CantReplaceLocationValue(Exception):
    """
    Raises, when can't change location value to another object
    """
