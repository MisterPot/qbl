from __future__ import (
    annotations,
)
from abc import (
    ABC,
    abstractmethod,
)
from typing_extensions import (
    Self,
)
from typing import (
    Dict,
    List,
    Type,
    Optional,
    Union,
    TypeVar,
    Any,
)


_T = TypeVar('_T')


class JsonBase(ABC):
    """
    Represents an object as JSON object
    """

    @classmethod
    @abstractmethod
    def create(cls, any_json: dict | list) -> Self:
        """
        Return this object with serialized fields
        :param any_json:
        :return:
        """


class JsonCollection(List[_T], JsonBase, ABC):
    """
    Represents a list of any JSON object in response
    """
    object_type: Type[_T] = None

    @classmethod
    def create(cls, any_json: list) -> Self:
        items = []

        for item in any_json:

            if issubclass(cls.object_type, (JsonCollection, JsonObject)):
                serialized_item = cls.object_type.create(item)
            else:
                serialized_item = cls.object_type(item)

            items.append(serialized_item)

        return cls(items)


def is_optional(annotation) -> bool:
    """
    Help function, for check is annotation are Optional
    :param annotation:
    :return:
    """
    origin = getattr(annotation, '__origin__', None)
    args = getattr(annotation, '__args__', ())
    return origin is Optional or (origin is Union and type(None) in args)


class JsonObject(JsonBase, ABC):

    """
    Represents a single JSON object in json response
    """

    @classmethod
    def keys(cls) -> Dict[str, Any]:
        """
        Returns actual keys of object.
        Includes inheritance
        :return:
        """
        fields = cls.__annotations__

        for base in cls.__bases__:

            if base != JsonBase:
                fields.update(getattr(base, '__annotations__', {}))

        return fields

    @classmethod
    def create(cls, any_json: dict) -> Self:

        if any_json is None:
            return

        obj = cls()

        for key_name, key_type in cls.keys().items():

            optional = False

            if is_optional(key_type):
                value = any_json.get(key_name)
                optional = True
            else:
                value = any_json[key_name]

            default_cast = lambda item: item
            cast_type = key_type

            if optional:
                cast_type = key_type.__args__[0]

            cast_to_type = cast_type.create if issubclass(cast_type, (JsonObject, JsonCollection)) else default_cast

            setattr(obj, key_name, cast_to_type(value))

        return obj
