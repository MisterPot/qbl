from __future__ import (
    annotations,
)

import copy
import inspect
import re
from dataclasses import (
    dataclass,
)
from functools import (
    lru_cache,
)
from typing import (
    Dict,
    Any,
    Callable,
    List,
    Tuple,
    Type,
)
from urllib.parse import (
    urlencode,
)

from .exceptions import (
    ValidationError,
    MissingMetaData,
    CantReplaceLocationValue,
)
from .enums import (
    Location,
)
from .typing import (
    RequestInfo,
)


CAN_REPLACE_LOCATIONS = [
    Location.DATA,
    Location.JSON,
]


@dataclass
class _FieldProperty:
    """
    Main field functionality object
    """

    field_name: str
    required: bool
    value: Any = None

    def __get__(self, instance, owner):
        return self.value

    def __set__(self, instance, value):
        self.value = value

    def make(self) -> Dict[str, Any]:
        """
        Returns a field data in a dict.
        If field is required and value is None -
        raises KeyError
        :return:
        """
        if self.value is None and self.required:
            raise KeyError(f'Field `{self.field_name}` is required')
        return {self.field_name: self.value}


class LocationNamespace(dict):

    def make(self) -> Dict[str, Any]:
        """
        Makes a data dict from fields names and fields values
        :return:
        """
        data = {}
        for field in self.values():
            data.update(field.make())
        return data


@dataclass
class Field:
    """
    Represents an input slot for a QueryBuilder as creation container
    """

    field_name: str = None
    default: Any = None
    required: bool = False
    validator: Callable[[Any], bool] = None
    location: Location = Location.PARAMS

    def __post_init__(self):
        default_validator = lambda value: True
        self.validator = self.validator or default_validator

    def __set_name__(self, owner, name):
        self.name = name
        self.field_name = self.field_name or name

    def __get__(self, instance, owner):
        if instance is None:
            return self
        return instance[self.location][self.name].__get__(instance, owner)

    def __set__(self, instance, value):
        if not self.validator(value):
            raise ValidationError(value, f'Incorrect value `{value}` for `{self.field_name}`')
        instance[self.location][self.name].__set__(instance, value)

    def create_property(self) -> _FieldProperty:
        """
        Creates property from this field
        :return:
        """
        return _FieldProperty(
            field_name=self.field_name or self.name,
            value=self.default,
            required=self.required
        )


class ExtendFieldMeta(type):

    """
    Metaclass, which allows to change a field value, if this field was
    in parent class
    """

    def __new__(mcs, name: str, bases: Tuple[Type], namespace: Dict[str, Any]):
        empty_object = type.__new__(mcs, name, bases, {})

        for attribute_name in list(namespace.keys()):

            attribute_value_base = getattr(empty_object, attribute_name, None)
            namespace_value = namespace[attribute_name]

            if isinstance(namespace_value, Field):
                namespace_value.__set_name__(owner=empty_object, name=attribute_name)
                setattr(empty_object, attribute_name, namespace_value)

            elif isinstance(attribute_value_base, Field):
                attribute_value_base = copy.copy(attribute_value_base)
                attribute_value_base.__set_name__(owner=empty_object, name=attribute_name)
                attribute_value_base.default = namespace.pop(attribute_name)
                setattr(empty_object, attribute_name, attribute_value_base)

            else:
                setattr(empty_object, attribute_name, namespace_value)

        return empty_object


class QueryBuilder:
    """
    Query builder can make a data to request
    """

    PATH: str
    METHOD: str

    def __init__(self, api_url: str):

        self._check_metadata('PATH')
        self._check_metadata('METHOD')

        self.api_url = api_url

        for field in self.external_fields().values():
            self._to_property_field(field=field)

    def _to_property_field(self, field: Field) -> None:
        """
        Automatically creates a LocationNamespace for property fields
        and creates property fields from external fields descriptors
        :param field:
        :return:
        """
        field_property = field.create_property()
        current_location = self[field.location]

        if current_location is None:
            setattr(self, field.location, LocationNamespace())

        self[field.location][field.name] = field_property

    def _check_metadata(self, key: str) -> None:
        """
        Help function, which can raise MissingMetaData error,
        when key data is missing in an instance
        :param key:
        :return:
        """
        if getattr(self, key, None) is None:
            raise MissingMetaData(f'{key} class attribute in {self.__class__.__name__} is missing')

    def __getitem__(self, item: Location) -> LocationNamespace:
        return getattr(self, item, None)

    def __setitem__(self, key: Location, value: Any) -> None:
        if key not in CAN_REPLACE_LOCATIONS:
            raise CantReplaceLocationValue(f'Can\'t set `{value}`. Location `{key}` is immutable')
        setattr(self, key, value)

    @classmethod
    @lru_cache()
    def external_fields(cls) -> Dict[str, Field]:
        """
        Returns available field inputs of the class
        :return:
        """
        fields = {}

        for attribute_name, attribute_value in inspect.getmembers(cls):
            if isinstance(attribute_value, Field):
                fields[attribute_name] = attribute_value

        return fields

    def make(self) -> RequestInfo:
        """
        Makes a request creation data
        :return:
        """
        kwargs = {
            location.value: (
                self[location].make()
                if isinstance(self[location], LocationNamespace)
                else self[location]
            )
            for location in list(Location)
            if self[location] and location != Location.URL
        }
        return RequestInfo(
            url=self.endpoint_url,
            method=self.METHOD,
            **kwargs
        )

    def _check_url_for_arguments(self) -> List[str]:
        """
        Help function, for checks arguments in url string
        :return:
        """
        return re.findall(r'\{([^}]+)}', self.PATH)

    @property
    def endpoint_url(self) -> str:
        """
        Current endpoint url
        :return:
        """
        arguments = (self[Location.URL] or LocationNamespace()).make()
        arguments_from_url = self._check_url_for_arguments()

        if len(arguments) != len(arguments_from_url):
            raise MissingMetaData('Different count of arguments in url and in class fields')

        if sorted(arguments) != sorted(arguments_from_url):
            raise MissingMetaData('Url passed arguments not matching with field names in class')

        formatted_path = self.PATH.format(**{
            argument_in_url: arguments[argument_in_url]
            for argument_in_url in arguments_from_url
        })

        return self.api_url + formatted_path

    @property
    def endpoint_url_with_params(self) -> str:
        """
        Makes url with params
        :return:
        """
        params = self[Location.PARAMS]

        if not params:
            return self.endpoint_url
        return f'{self.endpoint_url}?{urlencode(params.make(), doseq=True)}'


class UserAgentRequired(QueryBuilder):
    """
    A query, which requires a User-Agent header in query headers
    """
    agent = Field(
        field_name='User-Agent',
        required=True,
        location=Location.HEADERS
    )
