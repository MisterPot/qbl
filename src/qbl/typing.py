from typing import (
    TypedDict,
    Optional,
    Dict,
)


class RequestInfo(TypedDict):
    """
    Data to create request
    """
    url: str
    method: str
    params: Optional[Dict[str, str]]
    headers: Optional[Dict[str, str]]
    json: Optional[Dict[str, str]]
    data: Optional[Dict[str, str]]
