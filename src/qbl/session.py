from __future__ import (
    annotations,
)
import functools
from abc import (
    ABC,
    abstractmethod
)
from typing import (
    Type,
    Union,
    Callable,
    TypeVar,
    Coroutine,
    Any,
)

from .query_builder import (
    QueryBuilder
)


_InputType = TypeVar('_InputType')


def make_request_function(
        *,
        builder_cls: Type[QueryBuilder],
        response_object: Type[_InputType],
        base_url: str,
) -> Callable[..., Callable[..., _InputType | Coroutine[Any, Any, _InputType]]]:
    """
    Function decorator for making auto requests
    :param base_url: basic url for QueryBuilder
    :param response_object: object, which will serializes, when request performs
    :param builder_cls: builder, which will build request
    :return:
    """
    def function_wrapper(
            function: Callable[..., _InputType]
    ) -> Callable[..., _InputType | Coroutine[Any, Any, _InputType]]:

        @functools.wraps(function)
        def wrapper(self: Union[AsyncSession, SyncSession], **kwargs) -> _InputType | Coroutine[Any, Any, _InputType]:

            builder = builder_cls(base_url)

            for key, value in kwargs.items():
                setattr(builder, key, value)

            request_data = builder.make()

            if self.builder_to_arguments_serializer:
                request_data = self.builder_to_arguments_serializer(request_data)

            if isinstance(self, AsyncSession):

                async def async_wrapper() -> _InputType:
                    json_response = await self.request(**request_data)
                    return response_object.create(json_response)
                return async_wrapper()

            elif isinstance(self, SyncSession):
                json_resp = self.request(**request_data)
                return response_object.create(json_resp)

            raise ValueError('Unrecognized session parent class')

        return wrapper

    return function_wrapper


class AbstractSession(ABC):

    """
    Represents any transport session.
    Can be sync or async (requests, aiohttp, httpx etc.)
    """

    # If it's not None - must be something, which can deserialize made builder
    builder_to_arguments_serializer = None

    @abstractmethod
    def request(self, **kwargs) -> Union[list, dict]:
        """
        `builder_to_arguments_serializer` already realized in
        decorator `make_request_function`, you only
        :param kwargs:
        :return:
        """


class SyncSession(AbstractSession, ABC):

    """
    Representation of sync session for API
    """

    @abstractmethod
    def request(self, **kwargs) -> Union[list, dict]:
        ...


class AsyncSession(AbstractSession, ABC):

    """
    Representation async session for API
    """

    @abstractmethod
    async def request(self, **kwargs) -> Union[list, dict]:
        ...
