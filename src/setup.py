from setuptools import find_packages, setup

setup(
    name='qbl',
    version='1.0',
    packages=find_packages(),
    license='MIT',
    requires=[
        'typing_extensions'
    ]
)