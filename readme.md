### Query Building Library (qbl)

This is a simple library for build and serialize http requests in python


### Common example

```python
from qbl import (
    Field,
    QueryBuilder,
    Location
)


API_URL = 'https://my_api.com/api'


def is_adult(passed_age: int) -> bool:
    return passed_age > 18


class UserQuery(QueryBuilder):
    
    PATH = '/users'
    METHOD = 'GET'
    
    name = Field(
        required=True,
        location=Location.PARAMS
    )
    age = Field(
        required=True,
        location=Location.PARAMS,
        validator=is_adult
    )


if __name__ == '__main__':
    builder = UserQuery(API_URL)
    builder.name = 'James'
    builder.age = 19
    
    request_data = builder.make()

    print(request_data)

    # {
    #   'url': 'https://my_api.com/api/users',
    #   'method': 'GET',
    #   'params': {
    #       'name': 'James',
    #       'age': 19
    #   }
    # }
```

### QueryBuilder

For query builder can be created as instance, builder class must have at least this 2 attributes

<br/>METHOD - request method
<br/>PATH - endpoint path

### Field

Field is a proxy descriptor, which allow set a field to a builder.
When you create an instance of a builder, in instance created actual descriptors, through
which you can set and store field values.

Field initialization arguments: 
    <br/> _field_name_ : string, which will be in result dict, by default this a name of the field
    <br/> _default_ : default value of field, by default - None
    <br/> _required_ : is field required to input, by default - False
    <br/> _validator_ : function for value, which passes descriptor, by default - lambda item: True
    <br/> _location_ : a location, in which must be stored this descriptor property, by default - Location.PARAMS


### Locations

Locations Location.HEADERS, Location.PARAMS, Location.JSON and Location.DATA
have a same creation process, only Location.URL have a specific
behavior

```python

class RoomBuilder(QueryBuilder):
    
    PATH = '/{room_id}/'
    METHOD = 'GET'
    
    room = Field(
        required=True,
        location=Location.URL,
        field_name='room_id'
    )

```

In this example you see that field `room` have a `Location.URL` and
this field must correspond a path argument, named `room_id`.
In builder creation result you can see that:

```python
builder = RoomBuilder(API_URL)
builder.room = 12
print(builder.make())

# {
#   'url': 'https://my_api.com/api/12/',
#   'method': 'GET',
# }
```

### ExtendFieldMeta

This metaclass allow you to set a default field value, if field name
was in parent class

```python

from qbl import ExtendFieldMeta


class MyBuilder(QueryBuilder):
    
    METHOD = 'GET'
    PATH = '/root'
    
    user = Field(
        required=True,
        location=Location.JSON
    )


class BuilderWithField(MyBuilder, metaclass=ExtendFieldMeta):
    
    user = 'John'


builder1 = MyBuilder(API_URL)
builder2 = BuilderWithField(API_URL)

print(builder2.make())

# {
#   'url': 'https://my_api.com/api/root',
#   'method': 'GET',
#   'json': {
#       'user': 'John',
#   }
# }

print(builder1.make())

# KeyError: 'Field `user` is required'

```
